#pragma once

#include "std.h"

class Tile
{
	enum STATE {NORMAL = 0, START, END, OPEN, BLOCK, PATH, CLOSE};
	STATE state;
public:
	Tile();
	uint GetState();
	void SetState(uint);
	static uint GetWidth();
	static uint GetHeight();
};