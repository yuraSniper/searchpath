#pragma once

#include "std.h"

class Window
{
	HINSTANCE hInst;
	HWND hWnd;
	HDC hDC;
	HGLRC hRC;

	void setupWndClass(LPWSTR);
	void InitGL();
	void ResizeScene(uint, uint);

	static LRESULT __stdcall WndProc(HWND, uint, WPARAM, LPARAM);
public:
	Delegate onDraw, onKeyDown, onKeyUp,
		onButtonDown, onButtonUp, onMouseMove;
	Window(HINSTANCE, LPWSTR, uint, uint);
	HDC GetDC();
	HWND GetHWND();
};