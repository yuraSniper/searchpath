#include "Tile.h"

Tile::Tile()
{
	state = NORMAL;
}

uint Tile::GetState()
{
	return state;
}

void Tile::SetState(uint st)
{
	state = (STATE)st;
}

uint Tile::GetWidth()
{
	return 10;
}

uint Tile::GetHeight()
{
	return 10;
}