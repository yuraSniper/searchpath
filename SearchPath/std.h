#ifndef STD_H
#define STD_H

#define null NULL
typedef unsigned int uint;

#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "Glu32.lib")

#include <iostream>
#define _USE_MATH_DEFINES
#include <math.h>

using namespace std;

#endif

#include <Windows.h>
#include <WindowsX.h>
#include <gl\GL.h>
#include <gl\GLU.h>
#include "Window.h"
#include "Tile.h"
#include "TileRender.h"
#include "Core.h"
#include "Delegate.h"
#include "Input.h"
#include "List2.h"
#include "Search.h"

class Core;
class Window;
class Tile;
class TileRender;
class Input;
struct vertex;
class Search;