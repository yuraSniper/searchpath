#pragma once

#include "std.h"

class TileRender
{
	Core * core;
public:
	TileRender(Core *);
	void Render(uint, uint);
};