#include "Core.h"

Core::Core()
{
	field = null;
}

Core::~Core()
{
	DeleteField();
}

void Core::CreateField(uint WndWidth, uint WndHeight)
{
	width = WndWidth;
	height = WndHeight;
	wid = width / Tile::GetWidth();
	hei = height / Tile::GetHeight();

	if (field)
		DeleteField();

	field = new Tile[wid * hei];
}

void Core::DeleteField()
{
	if (field)
		delete [] field;
	field = null;
}

Tile & Core::GetTile(uint x, uint y)
{
	return field[y * width / Tile::GetWidth() + x];
}

uint Core::GetWidth()
{
	return width;
}

uint Core::GetHeight()
{
	return height;
}

uint Core::GetWid()
{
	return wid;
}

uint Core::GetHei()
{
	return hei;
}