#include "Window.h"

Window::Window(HINSTANCE _hInst, LPWSTR title, uint width, uint height)
{
	hInst = _hInst;
	RECT rc;
	rc.left = 0;
	rc.right = width;
	rc.top = 0;
	rc.bottom = height;
	AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW ^ WS_SIZEBOX ^ WS_MAXIMIZEBOX ^ WS_MINIMIZEBOX, null);
	setupWndClass(L"SearchPathWndClass");
	hWnd = CreateWindow(L"SearchPathWndClass", title, WS_OVERLAPPEDWINDOW ^ WS_SIZEBOX ^ WS_MAXIMIZEBOX ^ WS_MINIMIZEBOX,
		100, 50, rc.right - rc.left, rc.bottom - rc.top, null, null, hInst, null);
	SetProp(hWnd, L"This", this);
	ShowWindow(hWnd, SW_SHOWNORMAL);
	InitGL();
	GetClientRect(hWnd, &rc);
	ResizeScene(rc.right - rc.left, rc.bottom - rc.top);
}

HDC Window::GetDC()
{
	return hDC;
}

HWND Window::GetHWND()
{
	return hWnd;
}

void Window::setupWndClass(LPWSTR cls)
{
	WNDCLASS wc = {0};
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wc.hCursor = LoadCursor(null, IDC_ARROW);
	wc.hIcon = LoadIcon(null, IDI_APPLICATION);
	wc.hInstance = hInst;
	wc.lpfnWndProc = WndProc;
	wc.lpszClassName = cls;
	wc.style = CS_VREDRAW | CS_HREDRAW | CS_SAVEBITS;

	RegisterClass(&wc);
}

void Window::InitGL()
{
	PIXELFORMATDESCRIPTOR pfd = {0};
	int pf;

	hDC = ::GetDC(hWnd);

	pfd.nVersion = 1;
	pfd.nSize = sizeof(pfd);
	pfd.cColorBits = 32;
	pfd.dwFlags = PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW;
	pfd.iPixelType = PFD_TYPE_RGBA;

	pf = ChoosePixelFormat(hDC, &pfd);
	SetPixelFormat(hDC, pf, &pfd);

	hRC = wglCreateContext(hDC);
	wglMakeCurrent(hDC, hRC);

	glShadeModel(GL_SMOOTH);
	glClearColor(0, 0, 0, 0);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glClearDepth(1);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
}

void Window::ResizeScene(uint width, uint height)
{
	if (!height)
		height = 1;
	glViewport(0, 0, width, height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glOrtho(0, 1, -1, 0, 0, 1);
	//gluPerspective(45, (double)width/(double)height, 0.1, 100);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

LRESULT Window::WndProc(HWND hWnd, uint msg, WPARAM wParam, LPARAM lParam)
{
	Window * This = (Window *)GetProp(hWnd, L"This");
	static uint pos[2];
	static unsigned char key;
	switch (msg)
	{
		case WM_MOUSEMOVE:
			pos[0] = GET_X_LPARAM(lParam);
			pos[1] = GET_Y_LPARAM(lParam);
			This->onMouseMove(pos);
			return 0;
		case WM_KEYDOWN:
			key = LOBYTE(wParam);
			This->onKeyDown(&key);
			return 0;
		case WM_KEYUP:
			key = LOBYTE(wParam);
			This->onKeyUp(&key);
			return 0;
		case WM_LBUTTONDOWN:
		case WM_RBUTTONDOWN:
			key = (msg - WM_LBUTTONDOWN) % 2;
			This->onButtonDown(&key);
			return 0;
		case WM_LBUTTONUP:
		case WM_RBUTTONUP:
			key = (msg - WM_LBUTTONUP) % 2;
			This->onButtonUp(&key);
			return 0;
		case WM_PAINT:
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glLoadIdentity();

			This->onDraw(null);

			SwapBuffers(This->hDC);
			return 0;
		case WM_SIZING:
		case WM_SIZE:
			RECT rc;
			GetClientRect(hWnd, &rc);
			This->ResizeScene(rc.right - rc.left, rc.bottom - rc.top);
			return 0;
		case WM_DESTROY:
			RemoveProp(hWnd, L"This");
			PostQuitMessage(0);
			return 0;
		case WM_CREATE:
			return 0;
	}
	return DefWindowProc(hWnd, msg, wParam, lParam);
}