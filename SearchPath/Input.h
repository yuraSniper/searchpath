#pragma once

#include "std.h"

class Input
{
	Window * wnd;
	bool keys[256];
	bool keyPressed[256];
	bool buttons[2];
	uint pos[2];

	static void onKeyDown(void *, void *);
	static void onKeyUp(void *, void *);
	static void onButtonDown(void *, void *);
	static void onButtonUp(void *, void *);
	static void onMouseMove(void *, void *);
public:
	Input(Window *);
	bool GetKey(unsigned char);
	bool GetKeyPressed(unsigned char);
	bool GetButton(unsigned char);
	uint * GetMousePos();
	void SetTitle(double, int);
};