#include "std.h"

#define WIDTH 800
#define HEIGHT 600

int __stdcall process(void *);
void Draw(void *, void*);

int __stdcall WinMain(HINSTANCE hInst, HINSTANCE, LPSTR, int)
{
	MSG msg;
	HANDLE proc;
	void * param[2];
	Window * wnd = new Window(hInst, L"SearchPath", WIDTH, HEIGHT);
	Input * input = new Input(wnd);
	Core * core = new Core;
	TileRender * render = new TileRender(core);
	core->CreateField(WIDTH, HEIGHT);
	wnd->onDraw.add(Draw, render);
	param[0] = core;
	param[1] = input;

	proc = CreateThread(null, null, (LPTHREAD_START_ROUTINE)process, param, null, null);

	while (true)
	{
		if (PeekMessage(&msg, null, null, null, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				break;
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glLoadIdentity();

		Draw(render, null);

		SwapBuffers(wnd->GetDC());
		Sleep(1);
	}
	TerminateThread(proc, 0);
	return 0;
}

void Draw(void * param, void *)
{
	TileRender * render = (TileRender * )param;
	static uint wid = WIDTH / Tile::GetWidth(), hei = HEIGHT / Tile::GetHeight();
	for (int i = 0; i < wid; i++)
		for (int j = 0; j < hei; j++)
			render->Render(i, j);
}

int __stdcall process(void * param)
{
	Core * core = (Core *)(((void **)param)[0]);
	Input * input = (Input *)(((void **)param)[1]);
	Search s(core, input);
	uint state = 0;
	int sleep = 0;
	while (true)
	{
		if (input->GetKeyPressed('\b'))
			for (int i = 0; i < core->GetWid(); i++)
				for (int j = 0; j < core->GetHei(); j++)
					core->GetTile(i, j).SetState(0);
		if (input->GetKeyPressed(VK_NUMPAD1) || input->GetKeyPressed('1'))
			state = 1;
		if (input->GetKeyPressed(VK_NUMPAD2) || input->GetKeyPressed('2'))
			state = 2;
		if (input->GetKeyPressed(VK_NUMPAD3) || input->GetKeyPressed('3'))
			state = 4;
		if (input->GetKeyPressed(VK_NUMPAD0) || input->GetKeyPressed('0'))
			state = 0;
		if (input->GetButton(0))
		{
			uint * p;
			uint pos[2];
			p = input->GetMousePos();
			pos[0] = p[0] / Tile::GetWidth();
			pos[1] = p[1] / Tile::GetHeight();
			core->GetTile(pos[0], pos[1]).SetState(state);
		}
		if (input->GetButton(1))
		{
			uint * p;
			uint pos[2];
			p = input->GetMousePos();
			pos[0] = p[0] / Tile::GetWidth();
			pos[1] = p[1] / Tile::GetHeight();
			core->GetTile(pos[0], pos[1]).SetState(0);
		}
		if (input->GetKeyPressed(VK_UP))
		{
			sleep += 10;
			if (sleep < 0)
				sleep = 0;
		}
		if (input->GetKeyPressed(VK_DOWN))
		{
			sleep -= 10;
			if (sleep < 0)
				sleep = 0;
		}
		if (input->GetKeyPressed(13))
		{
			for (uint x = 0; x < core->GetWid(); x++)
				for (uint y = 0; y < core->GetHei(); y++)
				{
					switch (core->GetTile(x, y).GetState())
					{
						case 1:
						case 2:
						case 4:
							break;
						default:
							core->GetTile(x, y).SetState(0);
					}
				}
		}
		if (input->GetKeyPressed(' '))
		{
			for (uint x = 0; x < core->GetWid(); x++)
				for (uint y = 0; y < core->GetHei(); y++)
				{
					switch (core->GetTile(x, y).GetState())
					{
						case 1:
						case 2:
						case 4:
							break;
						default:
							core->GetTile(x, y).SetState(0);
					}
				}
			if (s.CheckField())
				s.StartSearch(sleep);
			
		}
		Sleep(10);
	}
	return 0;
}