#pragma once

#include "std.h"

struct vertex
{
	int x, y;
	double f, g, h;
	bool closed, opened;
	vertex * prevPath;
};

class Search
{
	Core * core;
	Input * input;

	vertex * GetStart(vertex *);
	vertex * GetFinish(vertex *);
public:
	Search(Core *, Input *);
	bool CheckField();
	void StartSearch(uint);
	void CalcF(vertex *, vertex *);
	void GetNeighbours(vertex *, vertex *, vertex *[]);
};