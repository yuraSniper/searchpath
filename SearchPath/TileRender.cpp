#include "TileRender.h"

TileRender::TileRender(Core * c)
{
	core = c;
}

void TileRender::Render(uint x, uint y)
{
	float r = 1, g = 0, b = 0;
	static double pixelw = 1 / (double)core->GetWidth(), pixelh = 1 / (double)core->GetHeight();
	switch (core->GetTile(x, y).GetState())
	{
		case 0:
			r = 0.2;
			g = 0.2;
			b = 0.2;
			break;
		case 1:
			r = 0;
			g = 1;
			b = 0;
			break;
		case 2:
			r = 1;
			g = 0;
			b = 0;
			break;
		case 3:
			r = 0;
			g = 0.2;
			b = 1;
			break;
		case 4:
			r = 0;
			g = 0;
			b = 0.6;
			break;
		case 5:
			r = 1;
			g = 1;
			b = 0;
			break;
		case 6:
			r = 0;
			g = 1;
			b = 1;
			break;
	}
	glBegin(GL_QUADS);
		glColor3f(r, g, b);
		glVertex2f((double)x * Tile::GetWidth() * pixelw, -(double)y * Tile::GetHeight() * pixelh);
		glVertex2f((double)x * Tile::GetWidth() * pixelw, -(double)((y + 1) * Tile::GetHeight() - 1) * pixelh);
		glVertex2f((double)((x + 1) * Tile::GetWidth() - 1) * pixelw, -(double)((y + 1) * Tile::GetHeight() - 1) * pixelh);
		glVertex2f((double)((x + 1) * Tile::GetWidth() - 1) * pixelw, -(double)y * Tile::GetHeight() * pixelh);
	glEnd();
}