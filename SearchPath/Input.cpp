#include "Input.h"

Input::Input(Window * w)
{
	wnd = w;
	for (int i = 0; i < 256; i++)
	{
		keys[i] = false;
		keyPressed[i] = false;
	}
	for (int i = 0; i < 2; i++)
	{
		buttons[i] = false;
		pos[i] = 0;
	}
	w->onButtonDown.add(onButtonDown, this);
	w->onButtonUp.add(onButtonUp, this);
	w->onKeyDown.add(onKeyDown, this);
	w->onKeyUp.add(onKeyUp, this);
	w->onMouseMove.add(onMouseMove, this);
}

bool Input::GetKey(unsigned char key)
{
	return keys[key];
}

bool Input::GetKeyPressed(unsigned char key)
{
	if (!keys[key] && keyPressed[key])
	{
		keyPressed[key] = false;
		return true;
	}
	return false;
}

bool Input::GetButton(unsigned char button)
{
	button %= 2;
	return buttons[button];
}

uint * Input::GetMousePos()
{
	return pos;
}

void Input::onKeyDown(void * param, void * key)
{
	static Input * This = (Input *)param;
	This->keys[*(unsigned char *)key] = true;
	This->keyPressed[*(unsigned char *)key] = true;
}

void Input::onKeyUp(void * param, void * key)
{
	static Input * This = (Input *)param;
	This->keys[*(unsigned char *)key] = false;
}

void Input::onButtonDown(void * param, void * p)
{
	static Input * This = (Input *)param;
	This->buttons[*(unsigned char *)p] = true;
}

void Input::onButtonUp(void * param, void * p)
{
	static Input * This = (Input *)param;
	This->buttons[*(unsigned char *)p] = false;
}

void Input::onMouseMove(void * param, void * p)
{
	static Input * This = (Input *)param;
	uint * pos = (uint *)p;
	This->pos[0] = pos[0];
	This->pos[1] = pos[1];
}

void Input::SetTitle(double x, int y)
{
	char * str = new char[64];
	sprintf(str, "SearchPath  Path len : %7.2f  time : %d", x, y);
	SetWindowTextA(wnd->GetHWND(), str);
}