#include "Search.h"

Search::Search(Core * c, Input * i)
{
	core = c;
	input = i;
}

bool Search::CheckField()
{
	uint s = 0, f = 0;
	for (int i = 0; i < core->GetWid(); i++)
		for (int j = 0; j < core->GetHei(); j++)
		{
			if (core->GetTile(i, j).GetState() == 1)
				s++;
			if (core->GetTile(i, j).GetState() == 2)
				f++;
		}
	return (s == 1 && f == 1);
}

vertex * Search::GetStart(vertex * field)
{
	for (int i = 0; i < core->GetWid(); i++)
		for (int j = 0; j < core->GetHei(); j++)
			if (core->GetTile(i, j).GetState() == 1)
			{
				return &field[j * core->GetWid() + i];
			}
	return null;
}

vertex * Search::GetFinish(vertex * field)
{
	for (int i = 0; i < core->GetWid(); i++)
		for (int j = 0; j < core->GetHei(); j++)
			if (core->GetTile(i, j).GetState() == 2)
			{
				return &field[j * core->GetWid() + i];
			}
	return null;
}

void Search::StartSearch(uint sleep)
{
	uint time = GetTickCount();
	List2 open(core->GetWid() * core->GetHei());
	vertex * field = new vertex[core->GetWid() * core->GetHei()];
	for (int x = 0; x < core->GetWid(); x++)
		for (int y = 0; y < core->GetHei(); y++)
		{
			field[y * core->GetWid() + x].x = x;
			field[y * core->GetWid() + x].y = y;
			field[y * core->GetWid() + x].prevPath = null;
			field[y * core->GetWid() + x].closed = (core->GetTile(x, y).GetState() == 4)? true : false;
			field[y * core->GetWid() + x].opened = false;
			field[y * core->GetWid() + x].f = 0;
		}
	vertex * start = GetStart(field);
	vertex * finish = GetFinish(field);
	vertex * current;
	vertex * neighbours[8];

	CalcF(start, finish);
	open.push(start);
	start->opened = true;
	CalcF(start, finish);

	while (!open.isEmpty())
	{
		Sleep(sleep);

		current = open.pop();
		current->opened = false;
		current->closed = true;

		if (current == finish)
		{
			input->SetTitle(finish->g, GetTickCount() - time);
			for (vertex * tmp = finish->prevPath; tmp != start; tmp = tmp->prevPath)
				core->GetTile(tmp->x, tmp->y).SetState(5);
			core->GetTile(start->x, start->y).SetState(1);
			core->GetTile(finish->x, finish->y).SetState(2);
			break;
		}

		if (current->x != start->x || current->y != start->y)
			core->GetTile(current->x, current->y).SetState(6);

		GetNeighbours(field, current, neighbours);


		for (int i = 0; i < 8; i++)
		{
			if (!neighbours[i] || neighbours[i]->closed)
				continue;

			double g = current->g;
			g += (neighbours[i]->y - current->y == 0 || neighbours[i]->x - current->x == 0)? 1 : M_SQRT2;

			if (!neighbours[i]->opened || g < neighbours[i]->g)
			{
				neighbours[i]->prevPath = current;
				CalcF(neighbours[i], finish);

				if (!neighbours[i]->opened)
				{
					open.push(neighbours[i]);
					neighbours[i]->opened = true;
					core->GetTile(neighbours[i]->x, neighbours[i]->y).SetState(3);
				}
				else
					open.update(neighbours[i]);
			}
		}
	}
	if (open.isEmpty())
		MessageBoxA(null, "There is no path!", "Error", MB_OK);
}

void Search::CalcF(vertex * nd, vertex * tmp)
{
	if (nd->prevPath)
	{
		nd->g = nd->prevPath->g;
		nd->g += (nd->prevPath->y - nd->y == 0 || nd->prevPath->x - nd->x == 0)? 1 : M_SQRT2;
	}
	else
		nd->g = 0;
	nd->h = sqrt(pow(abs(nd->x - tmp->x), 2.) + pow(abs(nd->y - tmp->y), 2.));
	//nd->h = (abs(nd->x - tmp->x) + abs(nd->y - tmp->y))*10;
	nd->f = nd->g + nd->h;
}

void Search::GetNeighbours(vertex * field, vertex * curr, vertex * n[8])
{
	int k = 0;
	for (int x = -1; x < 2; x++)
		for (int y = -1; y < 2; y++)
		{
			if (x == 0 && y == 0)
				continue;
			if (curr->x + x >= 0 && curr->x + x < core->GetWid() && curr->y + y >= 0 && curr->y + y < core->GetHei())
			{
				n[k] = &field[(curr->y + y) * core->GetWid() + curr->x + x];
			}
			else
				n[k] = null;
			k++;
		}
}