#pragma once

#include "std.h"

struct node
{
	vertex * v;
	node * next;
	node * prev;
};

class List2
{
	node * head;
	vertex * remove(vertex *);
public:
	List2(int);
	void push(vertex *);
	vertex * pop();
	void update(vertex *);
	bool isEmpty();
};