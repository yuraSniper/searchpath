#include "List2.h"

List2::List2(int n)
{
	head = null;
}

void List2::push(vertex * v)
{
	node * n = new node;
	n->v = v;
	if (head)
	{
		bool a = true, b = false;
		for (node * curr = head; a || curr != head; a = false, curr = curr->next)
			if (curr->v->f > v->f)
			{
				n->prev = curr->prev;
				n->next = curr;
				curr->prev->next = n;
				curr->prev = n;
				if (curr == head)
					head = head->prev;
				b = true;
				break;
			}
			if (!b)
			{
				n->prev = head->prev;
				n->next = head;
				head->prev->next = n;
				head->prev = n;
			}
	}
	else
	{
		head = n;
		head->next = head;
		head->prev = head;
	}
}

vertex * List2::pop()
{
	vertex * v = head->v;
	node * tmp = head;
	head->prev->next = head->next;
	head->next->prev = head->prev;
	if (head->next == head)
		head = null;
	else
		head = head->next;
	delete tmp;
	return v;
}

vertex * List2::remove(vertex * v)
{
	bool a = true;
	for (node * curr = head; a || curr!= head; a = false, curr = curr->next)
		if (curr->v == v)
		{
			curr->next->prev = curr->prev;
			curr->prev->next = curr->next;
			if (curr == head)
				head = head->next;
			delete curr;
			break;
		}
	return v;
}

void List2::update(vertex * v)
{
	push(remove(v));
}

bool List2::isEmpty()
{
	return (head == 0);
}