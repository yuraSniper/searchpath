#pragma once

#include "std.h"

class Core
{
	Tile * field;
	uint width, height, wid, hei;
public:
	Core();
	~Core();
	void CreateField(uint, uint);
	void DeleteField();
	Tile & GetTile(uint, uint);
	uint GetWidth();
	uint GetHeight();
	uint GetWid();
	uint GetHei();
};